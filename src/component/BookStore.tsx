import Book, { IBook } from "./Book";
import Cart from "./Cart";
import Filter from "./Filter";
import React, { useContext, useEffect, useState } from "react";
import { Button, Spin } from "antd";
import BookService from "../service/book-service";
import { AuthContext } from "../store/use-context";

const BookStore = () => {
  //Variables and State declarations
  const [isSpinning, setSpinning] = useState(false);
  let [books, setBooks] = useState<IBook[]>();
  let [pageNumber, setPageNumber] = useState<number>(1);
  const [quantity, setQuantity] = useState<any>(0);
  const [totalAmount, setTotalAmount] = useState<any>(0);
  //   let pageIsDisable = false;
  const store = useContext(AuthContext);
  const [pageIsDisable, setPageIsDisable] = useState(false);

  //Setting current page number
  const pageHandler = () => {
    pageNumber += 1;
    setPageNumber(pageNumber);
  };

  //rendering book for first time and accordingly
  useEffect(() => {
    setSpinning(true);
    if (pageNumber <= 1) {
      //for first time only
      BookService.fetchBooks({
        minPrice: store.minPrice,
        maxPrice: store.maxPrice,
        minQty: store.minQty,
        page: pageNumber,
      })
        .then((res) => setBooks(res))
        .finally(() => setSpinning(false));
    } else {
      BookService.fetchBooks({
        minPrice: store.minPrice,
        maxPrice: store.maxPrice,
        minQty: store.minQty,
        page: pageNumber,
      })
        .then((res) => {
          if (res.length === 0) {
            setPageIsDisable(true); //disabling button if response is empty
          } else {
            setBooks(books!.concat(res));
          }
        })
        // .then((prevState) => setBooks(books!.concat(prevState)))
        .finally(() => setSpinning(false));
    }
  }, [pageNumber]);

  //Filter logic
  const filter = () => {
    setSpinning(true);
    setBooks([]);
    setPageNumber(1);
    setPageIsDisable(false);  //set everything to initial state

    BookService.fetchBooks({
      minPrice: store.minPrice,
      maxPrice: store.maxPrice,
      minQty: store.minQty,
      page: 1,
    })
      .then((res) => setBooks(res))
      .catch((err) => alert(err))
      .finally(() => setSpinning(false));
  };

  return (
    <Spin spinning={isSpinning}>
      <Filter filter={filter} />
      {books?.map((book) => {
        return (
          <Book
            book={book}
            key={book.id}
            quantity={quantity!}
            setQuantity={setQuantity}
            totalAmount={totalAmount}
            setTotalAmount={setTotalAmount}
          />
        );
      })}
      <Button onClick={pageHandler} disabled={pageIsDisable} type={"primary"}>
        {" "}
        Load More{" "}
      </Button>
      <Cart
        quantity={quantity}
        setQuantity={setQuantity}
        totalAmount={totalAmount}
        setTotalAmount={setTotalAmount}
      />
    </Spin>
  );
};

export default BookStore;
