import {Card, Col, Row, Statistic} from "antd";
import { useEffect, useState } from "react";

interface IProps {
    quantity:any;
    setQuantity:any;
    setTotalAmount:any;
    totalAmount:any;
  }

const Cart: React.FC<IProps> = ({quantity, setQuantity, setTotalAmount, totalAmount}) => {    
    let [discountValue, setDiscountValue] = useState<any>(0);
    
    //Discount logic
    useEffect(() => {
        if(quantity > 10 || totalAmount > 1000){
            var disc = totalAmount * 0.1;
            setTotalAmount(totalAmount - disc);
            setDiscountValue(discountValue + disc);
        }else{
            setTotalAmount(totalAmount + discountValue);
            setDiscountValue(0);
        }
    }, [quantity] );
    

    return (
        <Card>
            <Row gutter={16}>
                <Col span={8}>
                    <Statistic title="Total Books to Order" value={quantity} />
                </Col>
                <Col span={8}>
                    <Statistic title="Total Discount" value={parseFloat(discountValue).toFixed(2)}  />
                </Col>
                <Col span={8}>
                    <Statistic title="Total Amount" value={parseFloat(totalAmount).toFixed(2)} />
                </Col>
            </Row>
        </Card>
    )
}
export default Cart;