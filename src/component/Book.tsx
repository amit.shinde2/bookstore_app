import { Card, Avatar, InputNumber, Row, Col } from "antd";

import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
  PlusOutlined,
  MinusOutlined,
} from "@ant-design/icons";
import { FC, useState } from "react";

const { Meta } = Card;

export interface IBook {
  id: number;
  title: string;
  description: string;
  totalQuantity: number;
  pricePerQty: number;
}
interface IProps {
  book?: IBook;
  quantity: any;
  setQuantity: any;
  setTotalAmount: any;
  totalAmount: any;
}

const Book: FC<IProps> = ({
  quantity,
  setQuantity,
  setTotalAmount,
  totalAmount,
  book = {},
}) => {
  //Variables and State declarations
  let [orderQuantity, setOrderQuantity] = useState<number>(0);
  let plusIsDisabled = false;
  let minusIsDisabled = false;
  let totalAmountAdd = orderQuantity * book.pricePerQty!;

  //Plus button Handler => Adds total quantity and total amount to cart
  const plusHandler = () => {
    setOrderQuantity(orderQuantity + 1);
    setQuantity(quantity + 1);
    setTotalAmount(totalAmount + book.pricePerQty!);
  };

  //Minus button Handler => Subtracts total quantity and total amount to cart
  const minusHandler = () => {
    orderQuantity -= 1;
    setOrderQuantity(orderQuantity);
    setQuantity(quantity - 1);
    setTotalAmount(totalAmount - book.pricePerQty!);
  };

  //Hiding plus and minus button accordingly
  if (orderQuantity <= 0) {
    minusIsDisabled = true;
  }

  if (orderQuantity === book.totalQuantity!) {
    plusIsDisabled = true;
  }

  return (
    <>
      <Card
        style={{ width: "80%", margin: "20px" }}
        actions={[
          <PlusOutlined onClick={plusHandler} hidden={plusIsDisabled} />,
          <InputNumber disabled={true} value={orderQuantity} />,
          <MinusOutlined onClick={minusHandler} hidden={minusIsDisabled} />,
        ]}
      >
        <Row>
          <Col span={4}>ID: {book.id}</Col>
          <Col span={8}>Name: {book.title}</Col>
          <Col span={12}>Description: {book.description}</Col>
        </Row>
        <Row>
          <Col span={12}>Total Quantity: {book.totalQuantity} Pc</Col>
          <Col span={12}>Price per Qty: INR {book.pricePerQty}</Col>
        </Row>

        <Row>
          <Col span={12}>Order Quantity: {orderQuantity} Pc</Col>
          <Col span={12}>Total Price: INR {totalAmountAdd}</Col>
        </Row>
      </Card>
    </>
  );
};
export default Book;
