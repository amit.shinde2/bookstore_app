import React, { createContext } from "react";
import store from './store';

type ContectProvideProps = {
    children: React.ReactNode
}

export const AuthContext = createContext(store);

export const AuthContextProvider = ({
    children,
}: ContectProvideProps) => {
    return <AuthContext.Provider value={store} >{children}</AuthContext.Provider>
}
