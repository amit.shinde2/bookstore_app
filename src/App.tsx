import './App.css';
import 'antd/dist/antd.css'
import BookStore from "./component/BookStore";
import { AuthContextProvider } from './store/use-context';

function App() {
    return (
        <AuthContextProvider>
        <div className="App">
            <BookStore/>
        </div>
        </AuthContextProvider>
    );
}

export default App;
