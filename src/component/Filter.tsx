import { Button, Card, Col, InputNumber, Row } from "antd";
import React, { useContext, useRef } from "react";

import { AuthContext } from "../store/use-context";

interface FuncProp {
  filter(): void;
}

const Filter: React.FC<FuncProp> = (props) => {
  //Varialbles and State declarations  
  const minRef = useRef<HTMLInputElement | null>(null);
  const maxRef = useRef<HTMLInputElement | null>(null);
  const quantityRef = useRef<HTMLInputElement | null>(null);
  const store = useContext(AuthContext);

  //Filter submit handler
  const filterHandler = (event: React.FormEvent) => {
    event.preventDefault();
    store.minPrice = Number(minRef.current?.value);
    store.maxPrice = Number(maxRef.current?.value);
    store.minQty = Number(quantityRef.current?.value);

    //Data validation for Filter
    if (
      store.minPrice < 0 ||
      store.maxPrice < 0 ||
      store.minQty < 0 ||
      store.minPrice > store.maxPrice
    ) {
      alert(
        "Please check value correctly(must be more than 0 and minPrice < maxPrice)"
      );
    } else {
      props.filter();
    }
  };

  return (
    <Card>
      <Row>
        <Col span={6}>
          <InputNumber id="min" addonBefore={"Min Price"} ref={minRef} />
        </Col>
        <Col span={6}>
          <InputNumber addonBefore={"Max Price"} ref={maxRef} />
        </Col>
        <Col span={6}>
          <InputNumber addonBefore={"Min Qty"} ref={quantityRef} />
        </Col>
        <Col span={6}>
          <Button onClick={filterHandler} type={"primary"}>
            Filter
          </Button>
        </Col>
      </Row>
    </Card>
  );
};

export default Filter;
